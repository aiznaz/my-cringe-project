package com.example.cringe.testms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestMsApplication.class, args);
    }

}
