package com.example.cringe.configms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigMsApplication.class, args);
	}

}
