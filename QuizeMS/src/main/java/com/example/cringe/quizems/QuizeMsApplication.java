package com.example.cringe.quizems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizeMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizeMsApplication.class, args);
	}

}
