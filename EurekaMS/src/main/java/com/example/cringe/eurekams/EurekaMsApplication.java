package com.example.cringe.eurekams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaMsApplication.class, args);
	}

}
