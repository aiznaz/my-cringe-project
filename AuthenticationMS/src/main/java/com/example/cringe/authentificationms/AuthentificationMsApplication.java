package com.example.cringe.authentificationms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthentificationMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthentificationMsApplication.class, args);
    }

}
